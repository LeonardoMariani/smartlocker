#include <inttypes.h>
#include <tm4c123gh6pm.h>
//~~LCD~~
//RS  PE1
//RW  PE2
//E   PE3

//D0  PB0
//D1  PB1
//D2  PB2
//D3  PB3
//D4  PB4
//D5  PB5
//D6  PB6
//D7  PB7

//~~KEYPAD~~
//A2-A5 (Colunas)
//C4-C7 (Linhas pullUP)

//~~SensorDistancia~~
//D2(Trigger) - D3(Echo)

//RELE
//PD6

//dataRequest
//PF1

#define EEPROM_INIT_OK 0
#define EEPROM_INIT_ERROR 2

uint8_t data_a;
uint8_t data_c;
uint32_t ctrl = 0x01;
char tecla;
char letra;
char funcao;
char senha[4];
char senhaTemp[4];
uint32_t i = 0;
uint32_t x;
uint32_t systickQtd = 0;
uint32_t tempoST = 40;//4000000  - 1s - ,000001 - 1uS
uint32_t tempoTotal = 0;
uint32_t distancia = 0;
uint32_t MAXCOUNT = 0;
uint32_t wait = 10000;
uint32_t btnPrecionado = 0;
uint32_t numConfirmacoes = 0;
uint32_t senhaOk = 0;
uint32_t okTrocaSenha = 0;
uint32_t testaInt = 0;
char readUART;

void configPortas(void);
void configInt(void);
void configLcd(void);
void delay(uint32_t);
void trataInt(void);
void pulso(void);
void writeLcd(char letra);
void systick(void);
char lerEEprom(uint8_t block, uint8_t offset);
void salvarEEprom(uint8_t block, uint8_t offset, char word);
uint32_t eempromInit(void);
void salvarSenha(char pass[4]);
uint32_t isNumero(char tecla);
uint32_t getDistancia();
void limparDisplay();
void senhaCerta();
void senhaErrada();
void alterarSenha();
void acionarRele();
uint32_t checkSenha();
uint32_t lerSenha();
void configUart(void);
char readChar(void);

uint32_t checkSenha(){
    uint32_t aux;
    lerSenha();

    for(aux = 0; aux < 4; aux++){
    if(senha[aux] == senhaTemp[aux]){
        numConfirmacoes++;
    }
    senhaTemp[aux] = 11;
  }

    return numConfirmacoes;
}
void acionarRele(){
    //ACIONAR RELE
    GPIO_PORTD_DATA_R = 1 << 6;
    delay(2000);
    GPIO_PORTD_DATA_R = 0 << 6;
}
void alterarSenha(){

    uint32_t aux;
    for(aux = 0; aux < 4; aux++){
    senha[aux] = senhaTemp[aux];
    senhaTemp[aux] = 11;
    //salva senha eeprom
    //funcao = '/';
  }
    salvarSenha(senha);
    limparDisplay();
    writeLcd('S');
    writeLcd('E');
    writeLcd('N');
    writeLcd('H');
    writeLcd('A');
    writeLcd('-');
    writeLcd('A');
    writeLcd('L');
    writeLcd('T');
    writeLcd('E');
    writeLcd('R');
    writeLcd('A');
    writeLcd('D');
    writeLcd('A');
    delay(1000);
    limparDisplay();

}
void senhaCerta(){
    limparDisplay();

    writeLcd('S');
    writeLcd('E');
    writeLcd('N');
    writeLcd('H');
    writeLcd('A');
    writeLcd('-');
    writeLcd('C');
    writeLcd('E');
    writeLcd('R');
    writeLcd('T');
    writeLcd('A');

    acionarRele();
    limparDisplay();

}
void senhaErrada(){
    limparDisplay();

    writeLcd('S');
    writeLcd('E');
    writeLcd('N');
    writeLcd('H');
    writeLcd('A');
    writeLcd('-');
    writeLcd('E');
    writeLcd('R');
    writeLcd('R');
    writeLcd('A');
    writeLcd('D');
    writeLcd('A');
    delay(1000);

    limparDisplay();

}
void limparDisplay(){
    GPIO_PORTB_DATA_R = 0b00000001;// limpa tela
    pulso();
}

uint32_t isNumero(char tecla){

    uint32_t isNumero = 0;
    if(tecla != 'A' && tecla != 'B' && tecla != 'C' && tecla != 'D' && tecla != '*' && tecla != '#'){
        isNumero = 1;
    }else{
        isNumero = 0;
    }

    return isNumero;
}
void salvarSenha(char pass[4]){
    uint32_t aux;
    uint32_t offset = 0;
    for(aux = 0; aux < 4; aux++){
        salvarEEprom(0, offset, pass[aux]);
        offset += 4;
        delay(10);
    }
}
uint32_t lerSenha(){
        uint32_t aux;
        uint32_t offset = 0;
        for(aux = 0; aux < 4; aux++){
            senha[aux] = lerEEprom(0, offset);
            offset += 4;
            delay(10);
        }
        return senha[aux];
}
void salvarEEprom(uint8_t block, uint8_t offset, char word) {
    while(EEPROM_EEDONE_R&EEPROM_EEDONE_WRBUSY==1||EEPROM_EEDONE_R&EEPROM_EEDONE_WKCOPY==1||EEPROM_EEDONE_R&EEPROM_EEDONE_WKERASE==1);

    EEPROM_EEBLOCK_R =  (uint8_t)(1<<block);
    EEPROM_EEOFFSET_R = (uint8_t)offset;
    EEPROM_EERDWR_R = (uint32_t)word;
}
char lerEEprom(uint8_t block, uint8_t offset){

    while(EEPROM_EEDONE_R&EEPROM_EEDONE_WRBUSY==1||EEPROM_EEDONE_R&EEPROM_EEDONE_WKCOPY==1||EEPROM_EEDONE_R&EEPROM_EEDONE_WKERASE==1);
    EEPROM_EEBLOCK_R = (uint8_t)(1<<block);
    EEPROM_EEOFFSET_R = (uint8_t)offset;

    return (uint32_t)EEPROM_EERDWR_R;
}
uint32_t eempromInit(){
    uint32_t ui32Aux;
    SYSCTL_RCGCEEPROM_R = 1;
    while(!SYSCTL_PREEPROM_R);
    for(ui32Aux=0;ui32Aux<2;ui32Aux++){}
    while(EEPROM_EEDONE_R){}
    if(EEPROM_EESUPP_R == 8 || EEPROM_EESUPP_R == 4)
    {
        return(EEPROM_INIT_ERROR);
    }

    SYSCTL_SREEPROM_R = 1;
    SYSCTL_SREEPROM_R = 0;
    for(ui32Aux=0;ui32Aux<2;ui32Aux++){}
    while(EEPROM_EEDONE_R){}
    if(EEPROM_EESUPP_R == 8 || EEPROM_EESUPP_R == 4)
    {
        return(EEPROM_INIT_ERROR);
    }

    return(EEPROM_INIT_OK);
}

uint32_t getDistancia(){

    uint32_t i;
    uint32_t dist;

    //uint32_t a = GPIO_PORTD_DATA_R;
    GPIO_PORTD_DATA_R = 1 << 2;// liga o trigger
    for(i = 0; i< 100; i++);//tempo ligado
    GPIO_PORTD_DATA_R = 0 << 2;//desliga o trigger

    while(GPIO_PORTD_DATA_R >> 3 == 0){}//echo == 0
    NVIC_ST_CTRL_R |= NVIC_ST_CTRL_INTEN;//liga o cronometro
    while(GPIO_PORTD_DATA_R >> 3 == 1){}// echo == 1 (recebendo sinal)
    NVIC_ST_CTRL_R = NVIC_ST_CTRL_ENABLE;//desliga o cronometro

    tempoTotal = systickQtd*tempoST; //Tempo total gasto para ir e voltar
    //velocidade do som = 34359cm/sec ou 0.034359cm/uS
    //porem � ida e volta entao 0.034359/2 = 0.01718
    dist = tempoTotal*0.01718; //dist = 40 ~> 10cm

    //zera os contadores
    systickQtd = 0;
    tempoTotal = 0;

    return dist;
}
void writeLcd(char letra){
    GPIO_PORTB_DATA_R = letra;

    GPIO_PORTE_DATA_R = 1 << 1 | 0 << 2 | 1 << 3;
    delay(10);
    GPIO_PORTE_DATA_R = 0 << 1 | 0 << 2 | 0 << 3;
}

void pulso(void){
    GPIO_PORTE_DATA_R = 0 << 1 | 0 << 2 | 1 << 3;
    delay(10);
    GPIO_PORTE_DATA_R = 0 << 1 | 0 << 2 | 0 << 3;
}
void configLcd(void){

    GPIO_PORTB_DATA_R = 0b00111000; // modo 8 bit - 2 linhas - 5x7 matriz
    pulso();

    GPIO_PORTB_DATA_R = 0b00001110;// Liga display-liga cursor
    pulso();

    GPIO_PORTB_DATA_R = 0b00000001;// limpa tela
    pulso();

    GPIO_PORTB_DATA_R = 0b00000110; // incrementa cursor para a direita
    pulso();

    GPIO_PORTB_DATA_R = 0b10000000; // posi��o do cursor
    pulso();
}
void configUart(void)
{
    SYSCTL_RCGCUART_R = 1 << 0;                 //ENABLE BIT 0
    GPIO_PORTA_AFSEL_R = 1 << 0  | 1 << 1;      //ENABLE A0,A1
    GPIO_PORTA_PCTL_R = 1 << 0 | 1 << 4;        //ENABLE PMC0 e PMC1

    //BRD = 16.000.000 / (16*9600) = 104.1666666666667
    //FBRD = (0.1666666666667 * 64 + 0.8) = 11.166668

    UART0_CTL_R = 0;                            //DISABLE UART
    UART0_IBRD_R = 104;                         //IBRD (valor inteiro)
    UART0_FBRD_R = 11;                          //FBRD (valor inteiro)
    UART0_LCRH_R = (0x3 << 5);                  //SIZE-WORD (8 bits)
    UART0_CC_R = 0x0;                           //CLOCK DO SISTEMA
    UART0_CTL_R = 1 << 0 | 1 << 8 | 1 << 9;     //ENABLE (UART/TX/RX)
}
void trataInt(){

    GPIO_PORTC_ICR_R = 1 << 4 | 1 << 5 | 1 << 6 | 1 << 7;
    GPIO_PORTF_ICR_R = 1 << 1;

    i++;

    uint8_t dataF = GPIO_PORTF_DATA_R;
    if((GPIO_PORTF_DATA_R >> 1 ) & 1 == 1){

      //GPIO_PORTA_DEN_R = 1 << 0 | 1 << 1;
      readUART = readChar();
     // GPIO_PORTA_DEN_R = 0 << 0 | 0 << 1;

      if(readUART == '1'){
          acionarRele();
          readUART = '0';
      }
    }


    data_a = GPIO_PORTA_DATA_R;
    data_c =  GPIO_PORTC_DATA_R >> 4 ;

    GPIO_PORTA_DATA_R = 0b011100;

for(x=0; x<4; x++){

    if(GPIO_PORTA_DATA_R == 0b011100 && ctrl == 0x01){//coluna 3
    ctrl = 0x02;
    GPIO_PORTA_DATA_R = 0b101100;
    tecla = '/';

        if(GPIO_PORTC_DATA_R >> 4 == 14){//
            tecla = '3';
        }else if(GPIO_PORTC_DATA_R >> 4 == 13){
            tecla = '6';
        }else if(GPIO_PORTC_DATA_R >> 4 == 11){
            tecla = '9';
        }else if(GPIO_PORTC_DATA_R >> 4 == 7){
            tecla = '#';
        }
    }else if(GPIO_PORTA_DATA_R == 0b101100 && ctrl == 0x02){//coluna 2
        ctrl = 0x03;
        GPIO_PORTA_DATA_R = 0b110100;

        data_c = GPIO_PORTC_DATA_R >> 4;
        if(GPIO_PORTC_DATA_R >> 4 == 14){
            tecla = '2';
        }else if(GPIO_PORTC_DATA_R >> 4 == 13){
            tecla = '5';
        }else if(GPIO_PORTC_DATA_R >> 4 == 11){
                    tecla = '8';
        }else if(GPIO_PORTC_DATA_R >> 4 == 7){
                    tecla = '0';
        }
     }else if(GPIO_PORTA_DATA_R == 0b110100 && ctrl == 0x03){//coluna 1
        ctrl = 0x04;
        GPIO_PORTA_DATA_R = 0b111000;

        data_c = GPIO_PORTC_DATA_R >> 4;
                if(GPIO_PORTC_DATA_R >> 4 == 14){
                    tecla = '1';
                }else if(GPIO_PORTC_DATA_R >> 4 == 13){
                    tecla = '4';
                }else if(GPIO_PORTC_DATA_R >> 4 == 11){
                    tecla = '7';
                }else if(GPIO_PORTC_DATA_R >> 4 == 7){
                    tecla = '*';
                }
      }else if(GPIO_PORTA_DATA_R == 0b111000 && ctrl == 0x04){//coluna 4
        ctrl = 0x01;
        GPIO_PORTA_DATA_R = 0b011100;
        data_c = GPIO_PORTC_DATA_R >> 4;

                if(GPIO_PORTC_DATA_R >> 4 == 14){//
                    tecla = 'A';
                    funcao = tecla;
                }else if(GPIO_PORTC_DATA_R >> 4 == 13){
                    tecla = 'B';
                    funcao = tecla;
                }else if(GPIO_PORTC_DATA_R >> 4 == 11){
                    tecla = 'C';
                    funcao = tecla;
                }else if(GPIO_PORTC_DATA_R >> 4 == 7){
                    tecla = 'D';
                    funcao = tecla;
                }
            }
    }
            data_a = GPIO_PORTA_DATA_R;
            data_c =  GPIO_PORTC_DATA_R >> 4;
            delay(120);

            if(tecla != '/'){
                //writeLcd(tecla);
                //tecla = '/';
                //GPIO_PORTA_DATA_R = 0b000000;
                //while(GPIO_PORTC_DATA_R >> 4 != 15){}//Enquanto a tecla estiver pressionada
                //btnPrecionado = 1;
                if(btnPrecionado == 0){

                    if(0){//if(funcao == 'A' && isNumero(tecla)){//Nova senha

                        if(senhaTemp[0] == 11){
                            senhaTemp[0] = tecla;
                            writeLcd('*');

                        }else if(senhaTemp[1] == 11){
                            senhaTemp[1] = tecla;
                            writeLcd('*');

                        }else if(senhaTemp[2] == 11){
                            senhaTemp[2] = tecla;
                            writeLcd('*');

                        }else if(senhaTemp[3] == 11){
                            senhaTemp[3] = tecla;
                            writeLcd('*');
                            funcao = '/';
                        }

                    }else if(funcao == 'B' && okTrocaSenha == 1){//Confirma a nova senha

                        if(senhaTemp[0] != 11 && senhaTemp[1] != 11 && senhaTemp[2] != 11 && senhaTemp[3] != 11 ){
                            alterarSenha();
                            okTrocaSenha = 0;
                        }else{
                            //faltam digitos para a senha
                            //funcao = '/';
                        }

                    }else if(funcao == 'C'){//Apagar tudo
                        funcao = '/';
                        uint32_t aux;
                        for(aux = 0; aux < 4; aux++){
                        senhaTemp[aux] = 11;
                      }
                        limparDisplay();

                    }else if(funcao == 'D'){//Entra senha

                        if(senhaTemp[0] != 11 && senhaTemp[1] != 11 && senhaTemp[2] != 11 && senhaTemp[3] != 11 ){

                            numConfirmacoes = checkSenha();
                             if(numConfirmacoes == 4){

                                 numConfirmacoes = 0;
                                 //funcao = '/';
                                 senhaOk = 1;
                                 senhaCerta();
                             }else{
                                 funcao = '/';
                                 numConfirmacoes = 0;
                                 //senha errada
                                 senhaOk = 0;
                                 uint32_t aux;
                                 for(aux = 0; aux < 4; aux++){
                                 senhaTemp[aux] = 11;
                               }
                                 senhaErrada();
                                 //funcao = '/';
                             }
                       }
                    }else if(isNumero(tecla) || funcao == 'A'){

                        if(senhaTemp[0] == 11){
                            senhaTemp[0] = tecla;
                            writeLcd('*');

                        }else if(senhaTemp[1] == 11){
                            senhaTemp[1] = tecla;
                            writeLcd('*');

                        }else if(senhaTemp[2] == 11){
                            senhaTemp[2] = tecla;
                            writeLcd('*');

                        }else if(senhaTemp[3] == 11){
                            senhaTemp[3] = tecla;
                            writeLcd('*');
                        }else if(funcao == 'A'){
                            numConfirmacoes = checkSenha();
                            if(numConfirmacoes == 4){
                                numConfirmacoes = 0;
                                funcao = '/';
                                limparDisplay();
                                okTrocaSenha = 1;
                            }else{
                                numConfirmacoes = 0;
                                senhaErrada();
                            }
                        }
                    }
                    if(funcao != 'A'){
                        funcao = '/';
                    }
                    btnPrecionado = 1;
                    tecla = '/';
                    GPIO_PORTA_DATA_R = 0b000000;
                }
            }else{
                btnPrecionado = 0;
            }

            GPIO_PORTA_DATA_R = 0b000000;

}
void systick(){
    systickQtd++;
}
int main(void) {

    configPortas();
    configLcd();
    configInt();
    configUart();

    GPIO_PORTA_DATA_R = 0b000000;
    GPIO_PORTD_DATA_R = 0 << 6;

    NVIC_ST_RELOAD_R = tempoST;
    NVIC_ST_CTRL_R = NVIC_ST_CTRL_ENABLE | NVIC_ST_CTRL_INTEN;//habilita

    senhaTemp[0] = 11;
    senhaTemp[1] = 11;
    senhaTemp[2] = 11;
    senhaTemp[3] = 11;
    //salvarSenha(senhaTemp);

    if(eempromInit() == 0){
        lerSenha();
        while(1){
            __asm("WFI");
            if(wait < 10){
                distancia = getDistancia();
                wait = 10000;
                if(distancia < 40){
                    acionarRele();
                }
            }
            wait--;
        }
    }else{
        writeLcd('e');
        writeLcd('r');
        writeLcd('r');
        writeLcd('o');
        writeLcd('-');
        writeLcd('e');
        writeLcd('e');
        writeLcd('p');
        writeLcd('r');
        writeLcd('o');
        writeLcd('m');

    }
	return 0;
}

void configInt(){

    NVIC_EN0_R = 1 << 2 | 1 << 30;//int na porta C e F

    GPIO_PORTC_IS_R = 0 << 4 | 0 << 5 | 0 << 6 | 0 << 7;//com borda
    GPIO_PORTC_IEV_R = 0 << 4 | 0 << 5 | 0 << 6 | 0 << 7;//descida
    GPIO_PORTC_IM_R = 1 << 4 | 1 << 5 | 1 << 6 | 1 << 7;//habilita int

    GPIO_PORTF_IS_R = 0 << 1;
    GPIO_PORTF_IEV_R = 1 << 1;
    GPIO_PORTF_IM_R = 1 << 1;

}
void configPortas(void){

    SYSCTL_RCGCGPIO_R = 1 << 0 | 1 << 1 | 1 << 2 | 1 << 3 | 1 << 4 | 1 << 5;

    GPIO_PORTA_DIR_R = 1 << 2 | 1 << 3 | 1 << 4 | 1 << 5;//A2~A5(coluna OUTPUT)
    GPIO_PORTB_DIR_R = 1 << 0 | 1 << 1 | 1 << 2 | 1 << 3 | 1 << 4 | 1 << 5 | 1 << 6 | 1 << 7;
    GPIO_PORTC_DIR_R = 0 << 4 | 0 << 5 | 0 << 6 | 0 << 7;//C4~C7(linha INPUT PULLUP)
    GPIO_PORTD_DIR_R = 1 << 2 | 0 << 3 | 1 << 6;// PD2 trigger - PD3 echo
    GPIO_PORTE_DIR_R = 1 << 1 | 1 << 2 | 1 << 3;                //PE1 (RS), PE2 (r/w) e PE3 (enable)
    GPIO_PORTF_DIR_R = 0 << 1;   //PF1 como input dataRequest

    GPIO_PORTA_DATA_R = 0;
    GPIO_PORTB_DATA_R = 0;
    GPIO_PORTC_DATA_R = 0;
    GPIO_PORTD_DATA_R = 0;
    GPIO_PORTE_DATA_R = 0;
    GPIO_PORTF_DATA_R = 0;

    //GPIO_PORTA_DEN_R = 1 << 0 | 1 << 1 | 1 << 2 | 1 << 3 | 1 << 4 | 1 << 5;
    GPIO_PORTA_DEN_R = 1 << 2 | 1 << 3 | 1 << 4 | 1 << 5;
    GPIO_PORTB_DEN_R = 1 << 0 | 1 << 1 | 1 << 2 | 1 << 3 | 1 << 4 | 1 << 5 | 1 << 6 | 1 << 7;
    GPIO_PORTC_DEN_R = 1 << 4 | 1 << 5 | 1 << 6 | 1 << 7;
    GPIO_PORTD_DEN_R = 1 << 2 | 1 << 3 | 1 << 6;
    GPIO_PORTE_DEN_R = 1 << 1 | 1 << 2 | 1 << 3;
    GPIO_PORTF_DEN_R = 1 << 1;
}

void delay(uint32_t n){
    uint32_t i,j;
    for(i=0;i<n;i++)
    for(j=0;j<1600;j++)
    {}
    //x++;
}
char readChar(void){
    char c;

    //GPIO_PORTA_DEN_R = 1 << 0 | 1 << 1;
    //while(((GPIO_PORTF_DATA_R >> 1) & 1) == 1){ //enquanto o bit estiver em 1
    //while((UART0_FR_R & (1<<4)) != 0);
    c = UART0_DR_R;
    //GPIO_PORTA_DEN_R = 0 << 0 | 0 << 1;
    //}
    return c;
}
